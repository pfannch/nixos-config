# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot = {
    kernelModules = [
      "kvm-intel"
      "thinkpad_acpi"
      "coretemp"
    ];

    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    cleanTmpDir = true;
  };

  fileSystems."/data" = { 
    device = "/dev/disk/by-uuid/643e9a8b-0a3c-4623-b139-73f04d8dfcbe";
    fsType = "ext4";
  };

  networking = {
    hostName = "nb-01-pfannch";
    networkmanager = {
      enable = true;
      wifi = {
        powersave = true;
        #backend = "iwd";
      };
    };
  };

  console = {
    keyMap = "us";
    font = "Lat2-Terminus16";
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  time.timeZone = "Europe/Berlin";

  security = {
    sudo = {
      enable = true;
      configFile = ''
        Defaults targetpw
        %wheel ALL = (ALL) ALL
      '';
    };
  };

  environment.systemPackages = with pkgs; [
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.zsh.enable = true;
  programs.sway.enable = true;

	fonts.fonts = with pkgs; [
		inconsolata
		font-awesome_5
	];

  hardware = {
    cpu.intel = {
      updateMicrocode = true;
    };

    enableAllFirmware = true;

    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
    };

    bluetooth = {
      enable = true;
      powerOnBoot = false;
    };
  };

  services.thinkfan = {
    enable = true;
    levels = ''
      (0,     0,      58)
      (1,     50,     62)
      (2,     52,     64)
      (3,     54,     66)
      (6,     56,     70)
      (7,     60,     85)
      (127,   80,     32767)
    '';
  };
  
  services.acpid = {
    enable = true;
  };

  services.tlp = {
    enable = true;
  };

  users = {
    mutableUsers = false;

    users.root = {
      hashedPassword = "$6$3HRupvpzrl/cFV$.kgRqbfhxN6/H.MSmJXcTmYmhiU4DNkYRXuzoJ/Sqdhu3ajqjJNqQ3XTH3Im3Qeks/zB6yUWcKpV7sRzmLK3Z1";
    };

    users.pfannch = {
      isNormalUser = true;
      extraGroups = [ 
      	"audio"
        "wheel" 
        "networkmanager" 
      ];
      uid = 1000;
      hashedPassword = "$6$kIkrMUOvhZ1W1$Q3cnHxOdj2XVPRfGVsOlTXiZ8/LqzQ9jQtmm1KMkn0A2omVTmPcWrK3lecLGX32OwsFqfshi/y7RiIqFJa8is1";
      shell = pkgs.zsh;
      packages = with pkgs; [
        home-manager
      ];
    };
  };

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
  };

  nixpkgs.config = {
    pulseaudio = true;
    allowUnfree = true;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09";

}
